Grading Sheet for HW7


Total: 77

Compiles    				20/20 pts
Comments 				8/10 pts (no header)
PrintMenu Method			10/10 pts
Checks for Invalid Characters	2/10 pts (should not quit the program if enter wrong thing, empty input is invalid)
getNumOfNonWSCharacters 	10/10 pts
getNumOfWords			5/10 pts (wrong character)
findText				7/10 pts(wrong result, ex: try "op" should return 1)
replaceExclamation			10/10 pts
shortenSpace				10/10 pts

-5 should not quit the program everytime, should keep asking for input from user. 
