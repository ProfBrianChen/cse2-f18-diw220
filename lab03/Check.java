import java.util.Scanner; // import the scanner
public class Check {  // main method required
public static void main (String [] args) { // begin main method
  Scanner scan = new Scanner(System.in); // make scan as the new scanner
  System.out.print("Enter the original cost of the check in the form xx.xx: "); // print the original cost in total that user have
  double checkCost = scan.nextDouble(); // accept user input of check cost
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");// print the percentage tip that user want to pay
  double tipPercent = scan.nextDouble(); // accept user input of percentage tip
  tipPercent /= 100; //We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner:");// print the number of people that went to dinner
int numPeople = scan.nextInt(); // accept user input of the number of people
double totalCost; // total cost 
double costPerPerson; // cost per person
int dollars;  //whole dollar amount of cost 
int dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10; // cost as dimes
pennies=(int)(costPerPerson * 100) % 10; // cost as dimes
System.out.println("Each person in the group owes $" + dollars + ',' + dimes + pennies); // print cost per person


  
  
  
  
  
} // end of main method
} // end of class
