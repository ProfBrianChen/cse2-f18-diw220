//DiWu CSE002 2018/12/4
import java.util.Scanner;
public class hw010 {
public static boolean condition(char [][] array, Scanner sc, char mark) {//method used to see if the input statisfies the condition
	
	if(mark == 'O') {
		System.out.println("Entering the position number the first person wishes to mark.");
	}else if (mark == 'X') {
		System.out.println("Entering the position number the second person wishes to mark.");
	}else {
		return false;
	}
	
	boolean cont = true;
	int num = -1;
	// get a number with the range [1,9]
	while ( cont ) {
		cont = sc.hasNextInt();
		if(cont) {
			num = sc.nextInt();
			if( num >= 1 && num <= 9) {
				cont = false;
				break;
			}
			System.out.println("It is out of bound");
			
		}else {
			System.out.println("enter an integer please");
			cont = true;
			sc.next();
		}
	}
            	   
    int row =(num-1) / 3;
    int col =(num-1) % 3;
    if(  '9' >= array[row][col] && array[row][col] >= '1') {
        	array[row][col]= mark;
    }else {
    	System.out.println("this postion was marked before. Please choose another one");
    	return false;
    }
    
    return true;
}

public static int check(char[][]array, int time) // method used to check who will win the game
{
	if(time < 5) {
		return 0;
	}
	//check row
	for(int i = 0; i < array.length;i++) { // i is row
		if(array[i][0] == array[i][1] && array[i][1] == array[i][2])
		{
			if(array[i][0] == 'O') {
				return 1;
			}else
			{
				return 2;
			}
		}
	}
	
	
	// check col
	for(int i = 0; i < array.length;i++) {
		if(array[0][i] == array[1][i] && array[1][i] == array[2][i])
		{
			if(array[0][i] == 'O') {
				return 1;
			}else
			{
				return 2;
			}
		}
	}
	
				if(array[0][0]==array[1][1]&&array[0][0]==array[2][2]) {
					if(array[0][0] == 'O') {
						return 1;
					}else
					{
						return 2;
					}
			}
				if(array[0][2]==array[1][1]&&array[0][2]==array[2][0]) {
					if(array[0][0] == 'O') {
						return 1;
					}else
					{
						return 2;
					}
			}
	
	// check diag
	
	
	// check draw
	if(time == 9) {
		return 3;
	}
	
	return 0; // no one win
}
public static void print(char[][] array) {//print array
	for (int i=0;i<array.length;i++) {
		for(int j=0; j<array[i].length;j++) 
		{
			System.out.print(array[i][j]);
			System.out.print(" ");
	
		}
		System.out.println();
	}
	}
public static void main (String [] args) {
	char [][] array = new char [3][3];// 3X3 array of char 
	char init='1';
	boolean cont = true;
	Scanner myscanner = new Scanner(System.in);//new scanner
	
	for (int i=0;i<array.length;i++) { 
		for(int j=0; j<array[i].length;j++) 
		{
			array[i][j]= init;
			System.out.print(array[i][j]);
			System.out.print(" ");
			init++;
		}
		System.out.println();
	}
	
	
	for(int i = 0; i < 9; i++) {
		char [] mark = {'O','X'};// 2 persons who use O and X respectivley
		if(!condition(array, myscanner, mark[i%2])) {// see which one uses o and which One uses X
			i--;
			continue;
		}
		int result = check(array, i+1);
		print(array);//print
//who will  win or draw
		if (result == 1) // first win
		{
			
			System.out.println("The frist person Wins");
			break;
		}else if (result == 2) // second win
		{
			System.out.println("The second person Wins");
			break;
		}else if (result == 3) // draw
		{
			System.out.println("draw");
		}
	
	}
	
}


}
