//CSE 002 DI WU 10/22
// I think the example of homework 6 is 11 x 11 not 10 x 10 for input = 10

import java.util.Scanner;// import scanner

public class EncryptedX { // class begins
	public static void main(String[] args) {// start of main method
	Scanner Scan = new Scanner(System.in);// scan is new scanner
	System.out.print("input is :");// require the input
	int input = Scan.nextInt();//require the input
	if (0>input||input>100) { // when input is not in the range, the system will exit
		System.out.println("this is invalid input.The system will quit." );
		System.exit(-1);
	}
	for(int i = 0; i < input; i++) {// when input is in the range. encrypted x will be created. determine the number of rows
		for (int j =0;j< input;j++) { // determine how many columns
			if(j==i || j==(input-i-1)) { // the frist and the last in each column will be space
			System.out.print(" ");
			
			}else { // print * 
			System.out.print("*");	
			}
			
			}
	System.out.println("");// change into next line
		}
		}//end of main method
}// end of class