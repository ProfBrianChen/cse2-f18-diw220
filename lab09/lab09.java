// Di Wu cse002 11/22
public class lab09 {
	public static int [] copy(int [] a) {
		int [] b = new int [a.length];
		for (int i = 0;i<a.length;i++) {
			b[i]=a[i];
		}
		return b;
	}
public static void inverter(int [] c) {
	int swap = 0;
	for (int i=0; i<c.length/2;i++) {
		swap = c[c.length-1-i];
		c[c.length-1-i]=c[i];
		c[i]=swap;
	}
}
public static int [] inverter2(int [] a) {
	int [] b = copy(a);
	inverter(b);
	return b;
}
public static void print(int [] a) {
	for (int i=0;i<a.length;i++) {
		System.out.print(a[i]+ " ");
	}
	
}
public static void main (String [] args) {
	int [] array0 = new int [10];
	for (int i=0;i<10;i++) {
		array0[i]=i+1;
		
	}
	int [] array1=copy(array0);
	int [] array2=copy(array0);
	inverter(array0);
	for (int i =0; i<10;i++) {
		System.out.print(array0[i] + " ");
	}
	System.out.println();
	inverter2(array1);
	for (int i =0; i<10;i++) {
		System.out.print(array1[i] + " ");
	}
	System.out.println();
	int [] array3=inverter2(array2);
	for (int i =0; i<10;i++) {
		System.out.print(array3[i] + " ");
	}
}
}
