// CSE2 DI WU 11/27
import java.util.Scanner;
public class RemoveElements{
	public static int[] randomInput(){// method used to random input
		int [] array = new int [10];
		for (int i =0; i<10;i++) {
			array[i] = (int)(Math.random()*10);
		}
		return array;
	}
	public static int[] delete(int [] list,int pos) {//method used to delete index
		if (pos>list.length-2) {
			System.out.println("it is out of bound. i will stop the program");
			System.exit(-1);
		}
		int [] p = new int [list.length-1] ;
		for (int i=0;i<pos;i++) {
				p[i]= list[i];
			}
		for (int i=pos;i<list.length-1;i++) {
			p[i]=list[i+1];
		}
		return p;
	}
	public static int[] remove(int [] list, int target) {// used to delte identical target
		int count=0;
		for (int i=0;i<list.length;i++) {
			if (list[i]==target) {
				count++;
			
			}
		}
		int [] w = new int [list.length - count];
		for (int i=0; i <list.length - count;i++) {
			if (list [i]!= target) {
				w[i]=list[i];
				
			}
		}
		return w;
	}
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}