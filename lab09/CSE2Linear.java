// CSE2 DI WU 11/27
1import java.util.Random;

import java.util.Scanner;
public class CSE2Linear {
	public static int binarySearch2(int[] list, int key) {
		// the method students learn from Prof.Carr's class
		int count=1;
		int low = 0;
		int high = list.length-1;
		while(high >= low) {
			int mid = (low + high)/2;
			if (key < list[mid]) {
				high = mid - 1;
			}
			else if (key == list[mid]) {
				return count;
			}
			else {
				low = mid + 1;
			}
			count++;
		}
	
		return -1;
	}

//learn from Prof.Carr' class
	public static int linearSearch(int[] list, int key) { 
		int counter =1;
		for (int i = 0; i < list.length; i++) 
		{
			if (key == list[i]) return counter;
			counter++;
		}

	return -1;
	}
	


	
	
	
	
	public static void main(String [] args) {// main method
		
	int [] array = new int [15];// intialize array
	boolean a= true;
	int input =0;
	
	Scanner myscanner= new Scanner(System.in);// make a new scanner
	System.out.print("Enter 15 ascending ints for final grades in CSE2: ");
	for (int i=0; i<15;i++)	{

		while (a){//testity if it is interger
		    
			 
			    boolean correct = myscanner.hasNextInt();
			    if (correct) {
			       input = myscanner.nextInt();
			    
			  a=false;
			      
			    }
			    else {
			      myscanner.next(); 
			      System.out.print("error, plz enter integers");
			      System.exit(-1);
			    }
			    
			  }
		
		if (input>100 || input <0) {// see if it is out of range
			System.out.println("the value is out of the range");
			System.exit(-1);
		}
		   array[i]=input;
		   
		   
		   if(i>0&&array[i]<array[i-1]) {// test if it is smaller than the previous one
			   System.out.println("the last value should be greater than the former one: error");
			   System.exit(-1);
		   }
		
		   a=true;
		
		
		
	}
	for(int i=0;i<15;i++) {//print array
		System.out.print(array[i]+" ");
	}
	System.out.println();
	
	System.out.print("Enter a grade to search for:");
	int key = myscanner.nextInt();
	int found = binarySearch2(array,key);//import the binarysearch method
	if (found == -1) {// to see if it is found or not
		System.out.println("the key is not found. I am so so sorry to hear that btw.");}
	else {// show  the times of iterations
		System.out.println("the key is found. The iterations are " + found + " times");
	}
	shuffle(array);//scramble the array
	System.out.println("Scrambled:   ");
	for(int i =0;i<15;i++) {
		System.out.print(array[i] + " ");// show the scrambled array
	}
	System.out.println();
	System.out.print("Enter a grade to search for: ");
	int key2 = myscanner.nextInt();
	int found2 = linearSearch(array,key2);//use the method linear search
	if (found == -1) {// if it is found or not
		System.out.println("the key is not found. I am so so sorry to hear that btw.");}
	else {//if it is found,see the iterations
		System.out.println("the key is found. The iterations are " + found + " times");
	}
	
	
	
	}//end of main method

public static void shuffle(int[] array) {//method used to scramble the array 
	for (int i = 0; i < array.length; i++) {
	  Random randgen= new Random();
	  int r = randgen.nextInt(15);
	  int temp = array[r];
	  array [r]= array [i];
	  array[i]= temp;
	}
}
}//end of class









