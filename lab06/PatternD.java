import java.util.Scanner;

public class PatternD {
	public static void main(String[] args) {
		Scanner myscanner = new Scanner(System.in);
		int x = 0;
		while (true) {

			System.out.println("please put an integer: ");
			boolean correct = myscanner.hasNextInt();
			if (correct) {
				x = myscanner.nextInt();
				break;

			} else {
				myscanner.next();
			}

		}

		for (int i=x; x>0; x--) {
			for (int j = x; j >0; j--) {
				System.out.print(j + " ");
			}
			System.out.println("");
		}

	}
}