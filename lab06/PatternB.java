import java.util.Scanner;

public class PatternB {
	public static void main(String[] args) {
		Scanner myscanner = new Scanner(System.in);
		int x = 0;
		while (true) {

			System.out.println("please put an integer: ");
			boolean correct = myscanner.hasNextInt();
			if (correct) {
				x = myscanner.nextInt();
				break;

			} else {
				myscanner.next();
			}

		}

		for (int i = x; x> 0; x--) {
			for (int j = 1; j < x + 1; j++) {
				System.out.print(j + " ");
			}
			System.out.println("");
		}

	}
}