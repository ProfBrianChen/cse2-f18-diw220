import java.util.Scanner;
import java.util.Random;
public class hw08{ 
	
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		 //suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = index - numCards;
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt(); 
		}  
	} 
	
	
	public  static String[] getHand(String[] cards, int index, int numCards) {
		String [] ret = new String[numCards];
		for(int i = 0 ; i< numCards;i++) {
			ret[i] = cards[index - i];
		}
		return ret;
 	}

	public static void printArray(String[] array) {
		for(int i = 0; i< array.length;i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
		return ;
	}
	
	public static void shuffle(String[] array) {
		for (int i = 0; i < array.length; i++) {
		  Random randgen= new Random();
		  int r = randgen.nextInt(52);
		  String temp = array[r];
		  array [r]= array [i];
		  array[i]= temp;
		}
	}
	
	
}
