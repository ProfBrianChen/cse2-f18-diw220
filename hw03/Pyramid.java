// CSE 002 Di Wu 9/14
import java.util.Scanner;//import scanner
public class Pyramid { // begin clas
  public static void main (String [] args){ //begin main method
    Scanner scan = new Scanner(System.in); // make scan as the new scanner
     System.out.print("The square side of the pyramid is (input length): "); // ask user input of length
    double length = scan.nextDouble(); // scanner length
    System.out.print("The height of the pyramid is (input height): "); // ask input of height
    double height = scan.nextDouble(); // 
    double volume = (1.0/3 * Math.pow(length,2) * height); // volume =1/3*S*h
   System.out.println("The volume inside the pyramid is: " + volume); // print output of volume
  }//end of main method
}//end of class
  