// a program that will pick a random card from the deck so you can practice your tricks alone. Use a random number generator to select a number from 1 to 52 (inclusive).  
// Di Wu CSE002 9/20
public class CardGenerator { // begin with class
 public static void main (String [] args) {  // main method
   String suit = "";  // string of suit
   String id = "";   // string of card identity
   int random = (int )(Math.random() * 52 + 1); // the variation of  variable is between 1 and 53
   
   
   // assign suit
   if (random >=1 && random <=13 ) {
 suit = "diamond"; }  
   if (random >=14&& random <=26) {
     suit = "clubs";
   }
   if (random >=27 && random <=39) {
     suit = "hearts";
   }
   if (random >=40&& random <=52) {
     suit = "spades";
   }
   
   // assign identity 
   switch (random) {
     case 1 : id = "Ace";
      break;
     case 2: id = "2";
       break;
     case 3: id = "3";
     break;
     case 4: id = "4";
      break;
     case 5: id = "5";
       break;
     case 6: id = "6";
       break;
     case 7: id = "7";
      break;
     case 8: id= "8";
       break;
     case 9: id = "9";
       break;
     case 10: id = "10";
       break;
     case 11: id = "Jack";
       break;
     case 12: id = "Queen";
     break;
     case 13: id = "King";
       break;
      case 14 : id = "Ace";
      break;
     case 15: id = "2";
       break;
     case 16: id = "3";
     break;
     case 17: id = "4";
      break;
     case 18: id = "5";
       break;
     case 19: id = "6";
       break;
     case 20: id = "7";
      break;
     case 21: id= "8";
       break;
     case 22: id = "9";
       break;
     case 23: id = "10";
       break;
     case 24: id = "Jack";
       break;
     case 25: id = "Queen";
     break;
     case 26: id = "King";
       break;
     case 27: id = "Ace";
      break;
     case 28: id = "2";
       break;
     case 29: id = "3";
     break;
     case 30: id = "4";
      break;
     case 31: id = "5";
       break;
     case 32: id = "6";
       break;
     case 33: id = "7";
      break;
     case 34: id= "8";
       break;
     case 35: id = "9";
       break;
     case 36: id = "10";
       break;
     case 37: id = "Jack";
       break;
     case 38: id = "Queen";
     break;
     case 39: id = "King";
       break;
     case 40: id = "Ace";
      break;
     case 41: id = "2";
       break;
     case 42: id = "3";
     break;
     case 43: id = "4";
      break;
     case 44: id = "5";
       break;
     case 45: id = "6";
       break;
     case 46: id = "7";
      break;
     case 47: id= "8";
       break;
     case 48: id = "9";
       break;
     case 49: id = "10";
       break;
     case 50: id = "Jack";
       break;
     case 51: id = "Queen";
     break;
     case 52: id = "King";
       break;
       
   }
 
   

   
   
   
 } // end of method
  
} // end of class