// Document the program
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) { 
      	// input data
	   	int secsTrip1=480;  // number of seconds for the first trip
       	int secsTrip2=3220;  // number of seconds for the second trip
		int countsTrip1=1561;  // number of counts for the frist trip
		int countsTrip2=9037; // number of counts for the second trip
      // intermediate variables and output data.
double wheelDiameter=27.0,  // the diameter of a wheel
  	PI=3.14159, // Pi used for calculating perimeter and area of a circle
  	feetPerMile=5280,  // feet per hour, one mile equals 5280 feet
  	inchesPerFoot=12,   // every foot equals 12 inches
  	secondsPerMinute=60;  // one minute equals = 60 seconds
	double distanceTrip1, distanceTrip2,totalDistance;  // distane of frist trip, second trip and the total trip
      
      // print out number of seconds and counts for each trip
       System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
      		//run the calculations; store the values.
		//calculation here. 
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // gives distance in miles
	totalDistance=distanceTrip1+distanceTrip2; // total distance in miles
      //Print out the output data. First trip and second trip in miles and totol trips in miles
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");



	}  //end of main method   
} //end of class
