//CSE002 NAME Di Wu HW2
///
/////
public class Arithmetic{  // main method required for java
  public static void main(String args[]){   // input data
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

 // total cost of each kind of item 
 double CostPants = numPants * pantsPrice; // Cost of pants
 double CostShirts = numShirts * shirtPrice; // Cost of shirts
 double CostBelts = numBelts * beltCost; // cost of belts
 // sales tax of each kind of item
 double TaxPants = paSalesTax * CostPants; // tax of pants
 double TaxShirts = paSalesTax * CostShirts; // tax of shirts
 double TaxBelts = paSalesTax * CostBelts; // tax of belts
 //total cost of purchases before tax
 double CostnoTax =(CostPants + CostShirts + CostBelts);
  // Total sales Tax
  double TotalTax = (TaxPants + TaxShirts + TaxBelts);
  // Total Cost including sales tax
  double TotalCost = (CostnoTax + TotalTax);
  
  //To eliminate extra digits and keep 2 digits after the demical point
  TotalTax = (int) TotalTax*100;
  TotalTax = TotalTax / 100.0;
  TotalCost = (int) TotalCost * 10;
  TotalCost = TotalCost / 100.0;
  
  // print out total cost of the purchases before tax, the total sales tax, and the total cost of the purchases including sales tax
   System.out.println("total cost of purchases before tax is " + CostnoTax + "$");
   System.out.println("the total sales tax is " + TotalTax + "$");
   System.out.println("total cost of the purchases including sales tax is " + TotalCost + "$");
  }
}
    
 